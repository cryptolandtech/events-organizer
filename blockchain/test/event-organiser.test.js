const CommunityEventsOrganiser = artifacts.require("CommunityEventsOrganiser");

const eventsMock = [
    [1, "First Event", 50, 0],
    [12, "Second Event", 20, 0.01],
    [33, "Third Event", 10, 0]
]

contract('CommunityEventsOrganiser', async (addresses) => {
    let owner = addresses[0];
    
    const mockEvents = (contract) => {
        let promises = [];
        for(let event of eventsMock) {
            promises.push(contract.createEvent(...event));
        }
        return Promise.all(promises);
    };

    const assertEvent = async (contract, eventId, eventData) => {
        // get event data from blockchain
        let eventIds = await(contract.getEventsIds());
        let [id, index, wpPostId, name, avPlaces, fee] = await contract.communityEventsMap(eventId);
        // check event fields
        assert.equal(wpPostId.toNumber(), eventData[0]);
        assert.equal(name, eventData[1]);
        assert.equal(avPlaces.toNumber(), eventData[2]);
        assert.equal(fee.toNumber(), eventData[3]);
        assert.equal(id.toNumber(), eventIds[index.toNumber()].toNumber());
    }
    
    it("should revert if sender is not admin for create/edit/removeEvent methods", async () => {
        let contract = await CommunityEventsOrganiser.new();
        // setup methods that needs to be tested
        let methodsTests =[
            {method: "createEvent", args:eventsMock[0]},
            {method: "editEvent", args:[1].concat(eventsMock[0])},
            {method: "removeEvent", args:[1]},
        ];

        // call each method and catch exceptions
        for (let test of methodsTests) {
            let error;
            try{
                await contract[test.method](...(test.args));
            } catch (e) {
                error = e;
            }
            assert.equal(
                error.message, 
                "VM Exception while processing transaction: revert",
                `${test.method} method`
            );
        }
    });

    it("should create event correctly", async() => {
        let contract = await CommunityEventsOrganiser.new();
        let event = eventsMock[0];
        // add admin role
        await contract.addAdmin(owner);
        // create events
        await mockEvents(contract);

        // check events auto generated id
        for(let i = 0; i < 3; i++) {
            assert.equal((await contract.communityEventsMap(i+1))[0].toNumber(), i+1);
        }

        // check event data
        await assertEvent(contract, 1, event);

        // check that eventsList array length is ok
        assert.equal((await contract.getEventsCount()).toNumber(), eventsMock.length);
    });

    it("should edit events correctly", async () => {
        let contract = await CommunityEventsOrganiser.new();
        let newEventData = [222, "Edited name", 333, 444];
        // add admin role
        await contract.addAdmin(owner);
        // create events
        await mockEvents(contract);

        // edit event with id 2
        await contract.editEvent(2, ...newEventData);

        // check changes, event with id:2 is at index 1 in eventList array
        await assertEvent(contract, 2, newEventData);

        // check that eventsList size remains the same (the same with eventsMock array)
        assert.equal((await contract.getEventsCount()).toNumber(), eventsMock.length);
    });

    it("should throw error when calling editEvent with an nonexistent eventId", async () => {
        let contract = await CommunityEventsOrganiser.new();
        // add admin role
        await contract.addAdmin(owner);

        let error;
        try{
            // edit event with id 2
            await contract.editEvent(2, ...eventsMock[0]);
        } catch (e) {
            error = e;
        }
        assert.equal(
            error.message, 
            "VM Exception while processing transaction: revert"
        );
    });

    it("should remove events smoothly", async () => {
        let contract = await CommunityEventsOrganiser.new();
        // add admin role
        await contract.addAdmin(owner);
        // create events
        await mockEvents(contract);

        // remove event with id 2
        await contract.removeEvent(2);
        // check that eventsList decreased
        assert.equal((await contract.getEventsCount()).toNumber(), eventsMock.length - 1);
        // check that all other events are ok, we need to check this because 
        await assertEvent(contract, 1, eventsMock[0]);
        await assertEvent(contract, 3, eventsMock[2]);

        // remove last event from the list
        await contract.removeEvent(3);
        // check that eventsList decreased
        assert.equal((await contract.getEventsCount()).toNumber(), eventsMock.length - 2);
        // check that all other events are ok, we need to check this because 
        await assertEvent(contract, 1, eventsMock[0]);

        // remove the last event from the list
        await contract.removeEvent(1);
        // check that eventsList decreased
        assert.equal((await contract.getEventsCount()).toNumber(), 0);
    });

    it("should not throw error when calling removeEvent with an nonexistent eventId", async () => {
        let contract = await CommunityEventsOrganiser.new();
        // add admin role
        await contract.addAdmin(owner);

        let error;
        try{
            // edit event with id 2
            await contract.removeEvent(2);
        } catch (e) {
            error = e;
        }
        assert.equal(
            error, 
            undefined
        );
    });
});