pragma solidity ^0.4.23;

import "./AdminUsers.sol";

contract CommunityEventsOrganiser is AdminUsers {
    uint internal idIncrement = 1;

    struct Participant {
        string name;
        string email;
        string phone;
    }

    struct CommunityEvent {
        uint id;
        uint index;
        uint wordpressPostId;
        string name;
        uint availablePlaces;
        uint256 fee;
        uint participantsCount;
        mapping(address => Participant) participants;
    }

    mapping(uint => CommunityEvent) public communityEventsMap;
    uint[] internal communityEventsMapKeys;

    function createEvent(
        uint wordpressPostId,
        string name,
        uint availablePlaces,
        uint256 fee
    ) public onlyAdmin returns(uint) {
        uint eventId = idIncrement++;
        
        communityEventsMap[eventId] = CommunityEvent({
            id: eventId,
            index: communityEventsMapKeys.push(eventId) - 1,
            wordpressPostId: wordpressPostId,
            name: name,
            availablePlaces: availablePlaces,
            fee: fee,
            participantsCount: 0
        });
    }

    function editEvent(
        uint eventId, 
        uint wordpressPostId,
        string name,
        uint availablePlaces,
        uint256 fee
    ) public onlyAdmin {
        require (communityEventsMap[eventId].id > 0);

        communityEventsMap[eventId] = CommunityEvent({
            id: eventId,
            index: communityEventsMap[eventId].index,
            wordpressPostId: wordpressPostId,
            name: name,
            availablePlaces: availablePlaces,
            fee: fee,
            participantsCount: communityEventsMap[eventId].participantsCount
        });
    }

    function removeEvent(uint8 eventId) public onlyAdmin {
        if (communityEventsMap[eventId].id > 0) {
            uint i = communityEventsMap[eventId].index;
            while (i < communityEventsMapKeys.length-1) {
                communityEventsMapKeys[i] = communityEventsMapKeys[i+1];
                communityEventsMap[communityEventsMapKeys[i+1]].index = i;
                i++;
            }
            communityEventsMapKeys.length--;
            delete communityEventsMap[eventId];
        }
    }

    function getEventsIds() public view returns(uint[]) {
        return communityEventsMapKeys;
    }

    function getEventsCount() public view returns(uint) {
        return communityEventsMapKeys.length;
    }

    function registerToEvent(
        uint8 eventId, 
        string name, 
        string email, 
        string phone
        ) public payable {
        require (bytes(name).length != 0);
        CommunityEvent storage eventObj = communityEventsMap[eventId];
        
        // value set to wei
        require (msg.value >= eventObj.fee);
        
        require (eventObj.availablePlaces - eventObj.participantsCount > 0);
 
        //check if the user is not already registered
        require (bytes(eventObj.participants[msg.sender].name).length == 0);
        eventObj.participants[msg.sender] = Participant({
            name: name,
            email: email,
            phone: phone
        });
        eventObj.participantsCount++;
    }

    // Participant can cancel his registration 
    function cancelEventRegistration(uint8 eventId) public {
        CommunityEvent storage eventObj = communityEventsMap[eventId];
 
        //check the user is already registered
        require (bytes(eventObj.participants[msg.sender].name).length > 0);

        delete eventObj.participants[msg.sender];
        eventObj.participantsCount--;
    }
}
