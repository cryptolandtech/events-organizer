pragma solidity ^0.4.23;

import "../node_modules/openzeppelin-solidity/contracts/ownership/Superuser.sol";

contract AdminUsers is Superuser {
    string public constant ROLE_ADMIN = "admin";

    function addAdmin(address newAdminAddress) public onlyOwnerOrSuperuser {
        addRole(newAdminAddress, ROLE_ADMIN);
    }

    function removeAdmin(address adminAddress) public onlyOwnerOrSuperuser {
        removeRole(adminAddress, ROLE_ADMIN);
    }

    function isAdmin(address _addr) internal view returns (bool) {
        return hasRole(_addr, ROLE_ADMIN);
    }

    modifier onlyAdmin()
    {
        checkRole(msg.sender, ROLE_ADMIN);
        _;
    }
}