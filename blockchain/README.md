Event Orgenizer DAPP - The Blockchain part
===

In this folder you will find the smart contract of the DAPP. We use Truffle to start a testnet locally.

## Project setup
- To test the smart contract locally you need:
    - latest version of nodejs

### Install dependencies
```
npm install
```

_**Note for Windows users:**_ If you're running Truffle on Windows, you may encounter some naming conflicts that could prevent Truffle from executing properly. Please see the section on [resolving naming conflicts](http://truffleframework.com/docs/advanced/configuration#resolving-naming-conflicts-on-windows) for solutions.

## Useful commands

- `npm run start`
It's an alias for `truffle develop`. See truffle documentation for details.

- `npm run compile`
Will compile the smart contracts. Also an alias of `truffle compile`.

- `npm run test`
Will run unit tests.

- `npm run test:coverage`
Will run unit tests and generate a coverage report at the end.