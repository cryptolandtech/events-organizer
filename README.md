Event Orgenizer DAPP
===

The idea of this project is to create a DAPP on Ethereum blockchain.
For start this is a project with educational purpose, we will keep it simple.

Our community is organising events, it would be nice to have an event organiser DAPP on the blockchain. Even if there are a lot of soulution out there for this purpose, and we don't really need blockchain for this, let's say we just want to be cool :)

So let's get to the requirements. 

The DAPP should have a public page where the future events are listed. Also each event should have a page with basic information and details about availability. Since we have a Wordpress website dedicated for events, we can store the post ids of the events in the blockchain and fetch the content to be displayed via wordpress api.  

When a user wants to participate to an event, he should input his name, email and optional the phone number. For the initial phase we won't have user accounts for each participant. After registration to event the participant will get a link where he can cancel his registration. The registration will trigger a contract function call. If the event needs payment the transaction with the function call should have the correct amount.

Administration area should be visible only to admins. In order to do that we need some sort of login. We can do that using signed messages. So on the contract we will have a list of administrators (ETH addresses), when an administrator wants to access admin area he will get a random message that needs to be signed with ETH address.

Administration functions: add/edit/delete event.

Project structure
---
There are 2 main parts of the project: the blockchain logic and the classic frontend app. for this purpose we have 2 folders:
- *blockchain* will host solidity contracts code and truffle setup for local test/development
- *fronteng* will host a small js app, the user facing app, probably implemented using react or preact
